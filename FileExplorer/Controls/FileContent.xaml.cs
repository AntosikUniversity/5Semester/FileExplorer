﻿using System.Windows.Controls;

namespace FileExplorer.Controls
{
    /// <summary>
    ///     Interaction logic for FileContent.xaml
    /// </summary>
    public partial class FileContent : UserControl
    {
        public FileContent()
        {
            InitializeComponent();
        }
    }
}