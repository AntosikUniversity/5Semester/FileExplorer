﻿using System.IO;
using System.IO.Compression;

namespace FileExplorer.Helpers
{
    public class GZip
    {
        public static void CompressFile(string path)
        {
            // https://msdn.microsoft.com/ru-ru/library/ms404280(v=vs.110).aspx
            using (var fileStream = File.OpenRead(path))
            {
                var fileInfo = new FileInfo(path);
                if (((File.GetAttributes(path) &
                      FileAttributes.Hidden) != FileAttributes.Hidden) & (fileInfo.Extension != ".gz"))
                    using (var compressedFileStream = File.Create(fileInfo.FullName + ".gz"))
                    {
                        using (var compressionStream = new GZipStream(compressedFileStream,
                            CompressionMode.Compress))
                        {
                            fileStream.CopyTo(compressionStream);
                        }
                    }
            }
        }

        public static void DecompressFile(string path)
        {
            // https://msdn.microsoft.com/ru-ru/library/ms404280(v=vs.110).aspx
            using (var originalFileStream = File.OpenRead(path))
            {
                var fileInfo = new FileInfo(path);
                var currentFileName = fileInfo.FullName;
                var newFileName = currentFileName.Remove(currentFileName.Length - fileInfo.Extension.Length);

                using (var decompressedFileStream = File.Create(newFileName))
                {
                    using (var decompressionStream = new GZipStream(originalFileStream, CompressionMode.Decompress))
                    {
                        decompressionStream.CopyTo(decompressedFileStream);
                    }
                }
            }
        }
    }
}