﻿namespace FileExplorer.Interfaces
{
    public interface IFile : INode
    {
        void OpenFile();
    }
}