﻿namespace FileExplorer.Interfaces
{
    public interface INode
    {
        string Name { get; set; }
        string Path { get; set; }
    }
}