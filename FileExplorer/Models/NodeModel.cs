﻿using System.Linq;
using FileExplorer.Interfaces;

namespace FileExplorer.Models
{
    public class NodeModel : INode
    {
        private string _path;
        private string _name;

        public NodeModel(string path)
        {
            Path = path;
            Name = path.Split('\\').Last();
        }

        public string Name
        {
            get => _name;
            set => _name = value;
        }

        public string Path
        {
            get => _path;
            set => _path = value;
        }
    }
}