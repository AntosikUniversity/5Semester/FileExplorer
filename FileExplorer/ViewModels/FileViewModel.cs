﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using FileExplorer.Interfaces;
using LiveCharts;
using LiveCharts.Wpf;

namespace FileExplorer.ViewModels
{
    public class FileViewModel : ViewModelBase, IFile
    {
        private string _name;
        private string _path;

        public FileViewModel(string filepath)
        {
            _path = filepath;
            _name = filepath.Split('\\').Last();

            SeriesCollection = null;
            Labels = null;
            Formatter = value => value.ToString("N");

            OpenFile();
        }

        public SeriesCollection SeriesCollection { get; set; }
        public string[] Labels { get; set; }
        public Func<double, string> Formatter { get; set; }

        public async void OpenFile()
        {
            using (var reader = File.OpenText(Path))
            {
                Content = await reader.ReadToEndAsync();
                OnPropertyChanged(nameof(Content));

                var content = Content.Replace("\r\n", " ");

                var words = new Dictionary<string, int>();
                foreach (var word in content.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries))
                {
                    if (words.ContainsKey(word)) words[word] += 1;
                    else if (word.Length > 1) words[word] = 1;
                }

                var topFive = words.OrderByDescending(s => s.Value).Take(5).ToDictionary(x => x.Key, x => x.Value);
                SeriesCollection = new SeriesCollection
                {
                    new ColumnSeries
                    {
                        Values = new ChartValues<int>(topFive.Values.ToArray())
                    }
                };
                Labels = topFive.Keys.ToArray();
            }
        }

        public string Name
        {
            get => _name;
            set => Change(ref _name, value);
        }

        public string Path
        {
            get => _path;
            set => Change(ref _path, value);
        }

        public string Content { get; private set; }
    }
}