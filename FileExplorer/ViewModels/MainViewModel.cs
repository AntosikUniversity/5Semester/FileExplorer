﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Input;
using FileExplorer.Helpers;
using FileExplorer.Models;

namespace FileExplorer.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        /* For async task running */
        private readonly object _lockObject = new object(); // for synchronization ObservableCollection

        private CancellationToken _cancelToken; // for cancel task
        private CancellationTokenSource _cancelTokenSource; // for cancel task

        private FileViewModel _openedFile;
        private SearchTypesEnum _searchType;
        private Task _task; // for cancel task

        public MainViewModel()
        {
            /* Init props */
            NodesFound = new ObservableCollection<NodeModel>();
            _openedFile = null;
            _searchType = SearchTypesEnum.Names;

            /* Init async task hanlders */
            BindingOperations.EnableCollectionSynchronization(NodesFound, _lockObject);
            _cancelTokenSource = new CancellationTokenSource();
            _cancelToken = _cancelTokenSource.Token;

            /* Init commands */
            Search = new Command(SearchFile);
            OpenFile = new Command(OpenSpecifiedFile);
            Compress = new Command(path =>
            {
                if (path is string pathStr) GZip.CompressFile(pathStr);
            });
            Decompress = new Command(path =>
            {
                if (path is string pathStr) GZip.DecompressFile(pathStr);
            });
        }

        /* SearchMenu */
        public static string[] SearchTypes { get; } = Enum.GetNames(typeof(SearchTypesEnum));

        public string SearchType
        {
            get => _searchType.ToString("G");
            set
            {
                if (Enum.TryParse(value, out SearchTypesEnum result))
                    Change(ref _searchType, result);
            }
        }

        /* Main collection */
        public ObservableCollection<NodeModel> NodesFound { get; }

        /* OpenedFile prop */ 
        public FileViewModel OpenedFile
        {
            get => _openedFile;
            set => Change(ref _openedFile, value);
        }

        /* Commands */
        public ICommand Search { get; }
        public ICommand OpenFile { get; }
        public ICommand Compress { get; }
        public ICommand Decompress { get; }

        // Opens File
        private void OpenSpecifiedFile(object path)
        {
            if (path is string filePath && File.Exists(filePath))
                OpenedFile = new FileViewModel(filePath);
        }

        // Searches for files
        private void SearchFile(object obj)
        {
            var expression = obj as string;

            NodesFound.Clear();

            if (_task != null)
            {
                _cancelTokenSource.Cancel();
                _cancelTokenSource = new CancellationTokenSource();
                _cancelToken = _cancelTokenSource.Token;
            }

            /* generating vars for searching */
            var search = _searchType == SearchTypesEnum.Content ? expression : null;
            expression = _searchType == SearchTypesEnum.Content
                ? "*.txt"
                : _searchType == SearchTypesEnum.Names
                    ? $"*{expression}*"
                    : expression;

            _task = Task.Run(() => SearchInDir("D:\\", expression, search, _cancelToken), _cancelToken);
        }

        /// <summary>
        /// Searches file in directory
        /// </summary>
        /// <param name="dirName">Full dir path</param>
        /// <param name="expression">expression for GetFiles && IsGroupAllow</param>
        /// <param name="search">Text to search</param>
        /// <param name="token">Token, needed for task cancel</param>
        private void SearchInDir(string dirName, string expression, string search, CancellationToken token)
        {
            if (token.IsCancellationRequested)
                return;
            try
            {
                foreach (var file in Directory.GetFiles(dirName, expression))
                {
                    if (_searchType == SearchTypesEnum.Names)
                        NodesFound.Add(new NodeModel(file));
                    else if (_searchType == SearchTypesEnum.Content && IsFileContains(file, search))
                        NodesFound.Add(new NodeModel(file));
                }
                foreach (var dName in Directory.GetDirectories(dirName))
                {
                    if (_searchType == SearchTypesEnum.Access && IsGroupAllow(dName, expression))
                        NodesFound.Add(new NodeModel(dName));

                    SearchInDir(dName, expression, search, token);
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine($"ERROR! {e.Message}");
            }
        }

        // Checks, is group allowed to folder
        private static bool IsGroupAllow(string folderPath, string groupName)
        {
            var ds = new DirectoryInfo(folderPath).GetAccessControl();
            return ds.GetAccessRules(true, true, typeof(NTAccount)).Cast<FileSystemAccessRule>()
                .Any(rule => rule.AccessControlType == AccessControlType.Allow &&
                             rule.IdentityReference.ToString().Contains(groupName));
        }

        // Checks, is file contains string
        private static bool IsFileContains(string filePath, string search)
        {
            using (var reader = File.OpenText(filePath))
            {
                var fileText = reader.ReadToEnd();
                return fileText.Contains(search);
            }
        }
    }

    public enum SearchTypesEnum
    {
        Names,
        Content,
        Access
    }
}